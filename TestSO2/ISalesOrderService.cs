﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TestSO2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISalesOrderService" in both code and config file together.
    [ServiceContract]
    public interface ISalesOrderService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string InsertSalesOrder(SalesOrderListContract values);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string UpdateSalesOrder(SalesOrderListContract values);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string DeleteSalesOrder(SalesOrderDeleteContract values);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        string GetDataSalesOrder(string all = null);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        string GetDataCustomer(string customer = null);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        string GetSalesOrder(string values);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json)]
        string GetSalesOrderDetail(string values);
    }

    [DataContract]
    public class SalesOrderListContract
    {
        [DataMember]
        public string so_id { get; set; }
        [DataMember]
        public string sales_order_no { get; set; }
        [DataMember]
        public string sales_order_date { get; set; }
        [DataMember]
        public string sales_address { get; set; }
        [DataMember]
        public string customer { get; set; }
        [DataMember]
        public List<SalesOrderDetailContract> details { get; set; }
    }

    [DataContract]
    public class SalesOrderDetailContract
    {
        [DataMember]
        public string sales_order_id { get; set; }
        [DataMember]
        public string item_name { get; set; }
        [DataMember]
        public int quantity { get; set; }
        [DataMember]
        public int price { get; set; }
        [DataMember]
        public int total { get; set; }
        [DataMember]
        public bool isDelete { get; set; }
    }

    [DataContract]
    public class SalesOrderDeleteContract
    {
        [DataMember]
        public string sales_order_id { get; set; }
    }
}
