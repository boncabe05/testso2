﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestSO2.Startup))]
namespace TestSO2
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
