﻿var temp_no = 0;
var edit_state = false;
var save_state = false;
var arrDetail = [];
var index = 0;
var curr_url = window.location.href;
var setUrl = new URL(curr_url);
var params = setUrl.searchParams.get("values");

function SalesDetail(so_id, sales_order_no, sales_order_date, sales_address, customer, details) {
    this.so_id = so_id;
    this.sales_order_no = sales_order_no;
    this.sales_order_date = sales_order_date;
    this.sales_address = sales_address;
    this.customer = customer;
    this.details = details;
}
function SalesOrderDetail(so_id, item_name, quantity, price, total, isDelete) {
    this.sales_order_id = (so_id === null || so_id === "") ? "" : so_id;
    this.item_name = item_name;
    this.quantity = quantity;
    this.price = price;
    this.total = total;
    this.isDelete = isDelete;
}
if (params !== null) {
    var urlGetSO = "../SalesOrderService.svc/GetSalesOrder?values=" + params;
    var urlGetSODetail = "../SalesOrderService.svc/GetSalesOrderDetail?values=" + params;
    var x = 1;
    var table = $('#tbl_sales_order').DataTable({
        'paging': false,
        'ordering': false,
        'info': false,
        'filter': false,
        'autoWidth': true,
        'serverside': true,
        'ajax': {
            'dataSrc': function (json) {
                return $.parseJSON(json);
            }
        },
        'columns': [{
            'name': 'no',
            'targets': 0,
            'data' : 'sales_order_id',
            'render': function (data, type, row, meta) {
                var html = "<div style='text-align:center;'>" + x + "<input type='hidden' class='sales_order_id' value='" + data + "'></div>";
                x++;
                return html;
            }
        }, {
            'name': 'item_name',
            'targets': 1,
            'data': 'item_name',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='text' readonly class='form-control input-item-name' placeholder='' style='width:100%' value='" + data + "'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'qty',
            'targets': 2,
            'data': 'quantity',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' readonly class='form-control input-qty number' pattern='[0-9]+' style='width:100%' value='" + data + "'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'price',
            'targets': 3,
            'data': 'price',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' readonly class='form-control input-price' pattern='[0-9]+' style='width:100%' value='" + data + "'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'total',
            'targets': 4,
            'data': 'total',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' readonly class='form-control input-total' pattern='[0-9]+' style='width:100%' value='" + data + "'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'action',
            'targets': 5,
            'render': function (data, type, row, meta) {
                var html = "<div class='col-md-12' style='text-align:center'><div class='form-group savediv' style='display:none'><button class='btn btn-primary save'style='width:100px' type='button'>Save</button>&nbsp;<button class='btn btn-warning cancel' style='width:100px' type='button'>Cancel</button></div><div class='form-group editdiv' ><button class='btn btn-success edit' style='width:100px' type='button'>Edit</button>&nbsp;<button class='btn btn-danger del' style='width:100px' type='button'>Del</button></div></div>";
                return html;
            }
        }]
    });

} else {
    var table = $('#tbl_sales_order').DataTable({
        'paging': false,
        'ordering': false,
        'info': false,
        'filter': false,
        'autoWidth': true,
        'columns': [{
            'name': 'no',
            'targets': 0,
            'render': function (data, type, row, meta) {
                var html = "<div style='text-align:center;'>" + temp_no + "<input type='hidden' class='sales_order_id' value='0'></div>";
                return html;
            }
        }, {
            'name': 'item_name',
            'targets': 1,
            'data': 'item_name',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='text' class='form-control input-item-name' placeholder='' style='width:100%' ><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'qty',
            'targets': 2,
            'data': 'quantity',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' class='form-control input-qty number' pattern='[0-9]+' style='width:100%' ><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'price',
            'targets': 3,
            'data': 'price',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' class='form-control input-price' pattern='[0-9]+' style='width:100%'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'total',
            'targets': 4,
            'data': 'total',
            'render': function (data, type, row, meta) {
                var html = "<div class='form-group'><input type='number' class='form-control input-total' pattern='[0-9]+' style='width:100%'><div class='has-error hide'></div></div>";
                return html;
            }
        }, {
            'name': 'action',
            'targets': 5,
            'render': function (data, type, row, meta) {
                var html = "<div class='col-md-12' style='text-align:center'><div class='form-group savediv'><button class='btn btn-primary save'style='width:100px' type='button'>Save</button>&nbsp;<button class='btn btn-warning cancel' style='width:100px' type='button'>Cancel</button></div><div class='form-group editdiv' style='display:none'><button class='btn btn-success edit' style='width:100px' type='button'>Edit</button>&nbsp;<button class='btn btn-danger del' style='width:100px' type='button'>Del</button></div></div>";
                return html;
            }
        }]
    });
}
$('#add_new_item').on('click', function (e) {
    e.preventDefault();
    temp_no = temp_no + 1;
    var add = table.row.add({
        'no': temp_no,
        'item_name': '',
        'qty': '',
        'price': '',
        'total': '',
        'action': ''
    }).draw().node();
    var cinput = $(add).closest('tr').children('td').find('input');
    $(add).closest('tr').children('td').find('.savediv').show();
    $(add).closest('tr').children('td').find('.editdiv').hide();
    $.each(cinput, function () {
        $(this).prop('readonly', false);
    });
});

$('#tbl_sales_order').on('click', '.cancel', function (e) {
    e.preventDefault();
    var cinput = $(this).closest('tr').children('td').find('input');
    $.each(cinput, function (i, v) {
        $(this).val('');
    });
    return;
});

$('#tbl_sales_order tbody').on('click', 'tr', function () {
    index = table.row(this).index();
    var qty = $('.input-qty', this);
    var price = $('.input-price', this);
    var total = $('.input-total', this);
    $('.input-qty', this).on('keydown keyup', function () {
        var _qty = isNaN(parseInt($(qty).val())) ? 0 : parseInt($(qty).val());
        var _price = isNaN(parseInt($(price).val())) ? 0 : parseInt($(price).val());
        var _total = isNaN(parseInt($(total).val())) ? 0 : parseInt($(total).val());
        _total = _qty * _price;
        $(total).val(_total);

    });
    $('.input-price', this).on('keydown keyup', function () {
        var _qty = isNaN(parseInt($(qty).val())) ? 0 : parseInt($(qty).val());
        var _price = isNaN(parseInt($(price).val())) ? 0 : parseInt($(price).val());
        var _total = isNaN(parseInt($(total).val())) ? 0 : parseInt($(total).val());
        _total = _qty * _price;
        $(total).val(_total);
    });
});

$('#tbl_sales_order').on('click', '.save', function (e) {
    e.preventDefault();
    var detail = new Array();
    var cinput = $(this).closest('tr').children('td').find('input');
    var formgroup = $(this).closest('tr').children('td').find('.form-group');
    var item_name_, qty_, price_, total_, so_id;
    so_id = cinput[0];
    item_name_ = cinput[1];
    qty_ = cinput[2];
    price_ = cinput[3];
    total_ = cinput[4];
    if (cinput.val() != '') {
        var sales_order_id = ($(so_id).val() != "undefined") ? $(so_id).val() : 0;
        detail = new SalesOrderDetail(sales_order_id, $(item_name_).val(), $(qty_).val(), $(price_).val(), $(total_).val(), false);
        arrDetail[index] = detail;
        $.each(cinput, function (i, v) {
            $(this).prop('readonly', true);
        });
        $.each(formgroup, function () {
            $(this).removeClass('has-error');
        });
        $(this).closest('tr').children('td').find('.savediv').hide();
        $(this).closest('tr').children('td').find('.editdiv').show();
    } else {
        alert('data tidak boleh kosong');
        cinput.prop('required', true);
        $.each(formgroup, function () {
            $(this).addClass('has-error');
        });
        return;
    }
});

$('#tbl_sales_order').on('click', '.edit', function (e) {
    e.preventDefault();
    $(this).closest('tr').children('td').find('.editdiv').hide();
    $(this).closest('tr').children('td').find('.savediv').show();
    var cinput = $(this).closest('tr').children('td').find('input');
    $.each(cinput, function () {
        $(this).prop('readonly', false);
    });
});

$('#tbl_sales_order').on('click', '.del', function (e) {
    e.preventDefault();
    var conf = confirm('Yakin ingin menghapus data ini?');
    var detail = new Array();
    var cinput = $(this).closest('tr').children('td').find('input');
    var item_name_, qty_, price_, total_, so_id;
    so_id = cinput[0];
    item_name_ = cinput[1];
    qty_ = cinput[2];
    price_ = cinput[3];
    total_ = cinput[4];
    if (conf) {
        var sales_order_id = ($(so_id).val() != "undefined") ? $(so_id).val() : 0;
        detail = new SalesOrderDetail(sales_order_id, $(item_name_).val(), $(qty_).val(), $(price_).val(), $(total_).val(), true);
        arrDetail[index] = detail;
        table.row($(this).parents('tr')).remove().draw();
        temp_no = temp_no - 1;
    } else {
        return;
    }
});

$('#saveSO').on('click', function (e) {
    e.preventDefault();
    var sales_order_no = $('#sales_order_no').val();
    var sales_order_date = $('#so_date').val();
    var sales_address = $('#address').val();
    var customer = $('#so_customer').val();
    var so_id = (params !== null) ? params : "";
    var sd_ = new SalesDetail(so_id, sales_order_no, sales_order_date, sales_address, customer, arrDetail);
    var data_ = JSON.stringify(sd_);
    var urlsave = (params !== null) ? "../SalesOrderService.svc/UpdateSalesOrder" : "../SalesOrderService.svc/InsertSalesOrder";
    $.ajax({
        url: urlsave,
        method: 'POST',
        contentType: 'application/json;charset=utf-8',
        data: data_,
        dataType: 'json',
        success: function (data) {
            var callback = JSON.parse(data);
            var success = callback.success;
            var message = callback.message;
            if (success) {
                alert(message);
                window.location.href = "/SalesOrderList/SalesOrderList.aspx";
            } else {
                alert(message);
            }
        },
        error: function (dataErr) {
            console.log('error : ' + JSON.stringify(dataErr));
        }
    });
});

$(document).ready(function () {
    var curr_url = window.location.href;
    var setUrl = new URL(curr_url);
    var params = setUrl.searchParams.get("values");
    var detail = new Array();
    if (params !== null) {
        $.getJSON(urlGetSO, function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, entry) {
                $('#sales_order_no').val(entry.sales_order_no);
                $('#so_date').val(entry.sales_order_date);
                $('#so_customer').val(entry.customer);
                $('#address').val(entry.sales_address);
            });
        });
        table.ajax.url(urlGetSODetail).load();
        $.getJSON(urlGetSODetail, function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, entry) {
                arrDetail[key] = entry
            });
        });
        let dropdown = $('#so_customer');
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Pilih</option>');
        dropdown.prop('selectedIndex', 0);
        const url = '../SalesOrderService.svc/GetDataCustomer';
        $.getJSON(url, function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, entry) {
                dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        });
    } else {
        let dropdown = $('#so_customer');
        dropdown.empty();
        dropdown.append('<option selected="true" disabled>Pilih</option>');
        dropdown.prop('selectedIndex', 0);
        const url = '../SalesOrderService.svc/GetDataCustomer';
        $.getJSON(url, function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, entry) {
                dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name));
            });
        });
    }
});
