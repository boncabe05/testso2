﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesOrder.aspx.cs" Inherits="TestSO2.SalesOrder.SalesOrder" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th {
            text-align: center;
        }
    </style>
    <div class="container body-content">
        <div class="" style="padding-top: 20px;">
            <legend>Header</legend>
            <div class="col-md-12">
                <form id="form_1" class="form">
                    <div class="form-group row">
                        <label for="sales_order_no" class="col-md-4 col-form-label">Sales Order No : </label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="sales_order_no" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="so_date" class="col-md-4 col-form-label">Sales Order Date : </label>
                        <div class="col-md-8">
                            <input type="date" class="form-control" id="so_date" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="customer" class="col-md-4 col-form-label">Customer : </label>
                        <div class="col-md-8">
                            <select class="form-control" id="so_customer"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label">Address : </label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" id="address" name="address" value=""></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="">
            <legend>Detail</legend>
            <div class="col-md-12">
                <div class="row">
                    <button class="btn btn-primary" type="button" id="add_new_item">ADD ITEM</button>
                </div>
                <div class="row">
                    <form id="formDetail">
                        <table class="table table-bordered tbl_sales_order" id="tbl_sales_order">
                            <thead>
                                <tr>
                                    <th style="width: 10%">NO.</th>
                                    <th style="width: 14%">Item Name</th>
                                    <th style="width: 14%">QTY</th>
                                    <th style="width: 14%">Price</th>
                                    <th style="width: 18%">Total</th>
                                    <th style="width: 30%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" style="text-align: right">Total:</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
                <div class="row">
                </div>
            </div>
        </div>
        <div class="">
            <div class="col-md-12">
                <div class="row" style="text-align: center">
                    <button class="btn btn-primary" type="button" id="saveSO">Save </button>
                    <button class="btn btn-primary" type="button">Cancel </button>
                </div>
            </div>
        </div>
    </div>
    <script src="/SalesOrder/salesorder.js"></script>
</asp:Content>
