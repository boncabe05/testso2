﻿//$('#tbl_sales_order_list').DataTable();
const url = '/SalesOrderService.svc/GetDataSalesOrder';
var x = 1;

var table = $('#tbl_sales_order_list').DataTable({
    "ordering": false,
    "info": false,
    "filter": true,
    "serverside": true,
    "paging":false,
    "ajax": {
        url: "/SalesOrderService.svc/GetDataSalesOrder",
        dataSrc: function (d) {
            return JSON.parse(d)
        }
    },
    'columns': [{
        'name': 'no',
        'targets': 0,
        'data': 'so_id',
        'render': function (data) {
            var html = x + "<input type='hidden' class='so_id' value='" + data + "'>";
            x++;
            return html;
        }
    }, {
        'name': 'so_no',
        'targets': 1,
        'data': 'so_no',
    }, {
        'name': 'order_date',
        'targets': 2,
        'data': 'order_date'
    }, {
        'name': 'customer',
        'targets': 3,
        'data': 'customer'
    }, {
        'name': 'action',
        'targets': 4,
        'data': 'so_id',
        'render': function (data, type, row, meta) {
            var html = "<div class='col-md-12' style='text-align:center'><div class='form-group edit'><a class='btn btn-primary edit'style='width:100px' type='button' href='/SalesOrder/SalesOrder?values=" + data + "'>EDIT</a>&nbsp;<button class='btn btn-danger delete' style='width:100px' type='button'>DEL</button></div></div>";
            return html;
        }
    }]
});

$('#btn_search').on('click', function (e) {
    var order_date = $('#order_date').val();
    var keywords = $("#keywords").val();
    console.log(order_date);
    e.preventDefault();
    if (order_date !== null) {
        table.search(order_date).draw();
    } else {
        table.search(keywords).draw();
    }
});

$('#tbl_sales_order_list_filter').hide();

$('#tbl_sales_order_list').on('click', '.delete', function (e) {
    e.preventDefault();
    var conf = confirm('Yakin ingin menghapus data ini?');
    var cinput = $(this).closest('tr').children('td').find('input');
    if (conf) {
        var data_ = { "sales_order_id" : $(cinput).val() };
        $.ajax({
            url: "../SalesOrderService.svc/DeleteSalesOrder",
            method: 'POST',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(data_),
            dataType: 'json',
            success: function (data) {
                var callback = JSON.parse(data);
                var success = callback.success;
                var message = callback.message;
                if (success) {
                    alert(message);
                    window.location.href = "/SalesOrderList/SalesOrderList.aspx";
                } else {
                    alert(message);
                }
            },
            error: function (dataErr) {
                console.log('error : ' + JSON.stringify(dataErr));
            }
        });
    } else {
        return;
    }
    return;
});