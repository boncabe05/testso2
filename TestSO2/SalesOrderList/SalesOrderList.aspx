﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesOrderList.aspx.cs" Inherits="TestSO2.SalesOrderList.SalesOrderList" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        table thead tr th, table tbody tr td {
            text-align:center;
        }
    </style>
    <div class="container body-content">
        <div class="" style="padding-top:20px;padding-bottom:20px">
            <legend>Header</legend>
            <div class="col-md-9">
                <form id="form_1" class="form">
                    <div class="form-group row">
                        <label for="sales_order_no" class="col-sm-2 col-form-label">Keywords : </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="keywords" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Sales Order : </label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="order_date" >
                        </div>
                    </div>
                    <div class="right" style="text-align:right">
                        <button type="button" class="btn btn-primary right" id="btn_search">Search</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-12" style="">
            <legend>Detail</legend>
            <div class="col-md-12">
                <div class="row">
                    <a class="btn btn-primary" href="<%= Page.ResolveUrl("~/SalesOrder/SalesOrder.aspx") %>">ADD SO</a>
                </div>
                <div class="row">
                    <table class="table tbl_sales_order_list" id="tbl_sales_order_list">
                        <thead>
                            <tr>
                                <th>NO.</th>
                                <th>Sales Order No.</th>
                                <th>Order Date</th>
                                <th>Customer</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="/SalesOrderList/salesorderlist.js"></script>
</asp:Content>
