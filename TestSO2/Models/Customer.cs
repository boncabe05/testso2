﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSO2.Models
{
    public class Customer
    {
        public int COM_CUSTOMER_ID { get; set; }
        public string CUSTOMER_NAME { get; set; }
    }
}