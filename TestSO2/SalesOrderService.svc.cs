﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;


namespace TestSO2
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SalesOrderService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SalesOrderService.svc or SalesOrderService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode
    = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SalesOrderService : ISalesOrderService
    {

        private SO_ClassesDataContext db = new SO_ClassesDataContext();

        public string InsertSalesOrder(SalesOrderListContract values)
        {
            MessageNotif message = new MessageNotif();
            SALES_SO_LITEM objSalesSOLItem = new SALES_SO_LITEM();
            db = new SO_ClassesDataContext();
            SALES_SO objSalesSO = new SALES_SO
            {
                SO_NO = values.sales_order_no,
                ORDER_DATE = Convert.ToDateTime(values.sales_order_date),
                ADDRESS = values.sales_address,
                COM_CUSTOMER_ID = Convert.ToInt32(values.customer)
            };
            try
            {
                db.SALES_SOs.InsertOnSubmit(objSalesSO);
                db.SubmitChanges();
                int id_ = objSalesSO.SALES_SO_ID;
                foreach (var det_ in values.details)
                {
                    objSalesSOLItem = new SALES_SO_LITEM
                    {
                        SALES_SO_ID = id_,
                        ITEM_NAME = det_.item_name,
                        QUANTITY = det_.quantity,
                        PRICE = det_.price,
                    };
                    try
                    {
                        db.SALES_SO_LITEMs.InsertOnSubmit(objSalesSOLItem);
                        db.SubmitChanges();
                        message.success = true;
                        message.message = "Insert success";
                    }
                    catch (Exception ex)
                    {
                        message.message = "Insert failed, litem" + ex.Message;
                    }
                }
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "Insert failed, " + e.Message;
            }

            return JsonConvert.SerializeObject(message);
        }

        public string UpdateSalesOrder(SalesOrderListContract values)
        {
            MessageNotif message = new MessageNotif();
            int soID = Convert.ToInt32(values.so_id);
            SALES_SO_LITEM objSalesSOLItem = new SALES_SO_LITEM();
            db = new SO_ClassesDataContext();
            SALES_SO _so = db.SALES_SOs.Single(s => s.SALES_SO_ID == soID);
            _so.SO_NO = values.sales_order_no;
            _so.ADDRESS = values.sales_address;
            _so.COM_CUSTOMER_ID = Convert.ToInt32(values.customer);
            _so.ORDER_DATE = Convert.ToDateTime(values.sales_order_date);
            db.SubmitChanges();
            var detail = values.details;
            foreach (var x in detail)
            {
                int _solid = Convert.ToInt32(x.sales_order_id);
                var exist = db.SALES_SO_LITEMs.Where(d => d.SALES_SO_LITEM_ID == _solid && d.SALES_SO_ID == soID).Any();
                if (exist)
                {
                    if (!x.isDelete)
                    {
                        objSalesSOLItem = db.SALES_SO_LITEMs.Single(d => d.SALES_SO_LITEM_ID == _solid && d.SALES_SO_ID == soID);
                        objSalesSOLItem.ITEM_NAME = x.item_name;
                        objSalesSOLItem.PRICE = Convert.ToInt32(x.price);
                        objSalesSOLItem.QUANTITY = Convert.ToInt32(x.quantity);
                    }else
                    {
                        objSalesSOLItem = db.SALES_SO_LITEMs.Single(d => d.SALES_SO_LITEM_ID == _solid && d.SALES_SO_ID == soID);
                        db.SALES_SO_LITEMs.DeleteOnSubmit(objSalesSOLItem);
                    }
                    try
                    {
                        db.SubmitChanges();
                        message.message = "updated succesfully";
                        message.success = true;
                    }
                    catch (Exception ex)
                    {
                        message.message = "updated failed " + ex.Message;
                        message.success = false;
                    }
                }
                else 
                {
                    objSalesSOLItem = new SALES_SO_LITEM
                    {
                        SALES_SO_ID = Convert.ToInt32(values.so_id),
                        ITEM_NAME = x.item_name,
                        QUANTITY = x.quantity,
                        PRICE = x.price
                    };
                    db.SALES_SO_LITEMs.InsertOnSubmit(objSalesSOLItem);
                    try
                    {
                        db.SubmitChanges();
                        message.message = "updated succesfully";
                        message.success = true;
                    }
                    catch (Exception ex)
                    {
                        message.message = "updated failed " + ex.Message;
                        message.success = false;
                    }
                }
            }

            var data = JsonConvert.SerializeObject(message);

            return data;
        }

        public string DeleteSalesOrder(SalesOrderDeleteContract values)
        {
            MessageNotif message = new MessageNotif();
            int soID = Convert.ToInt32(values.sales_order_id);
            db = new SO_ClassesDataContext();
            SALES_SO _so = db.SALES_SOs.Single(s => s.SALES_SO_ID == soID);
            db.SALES_SOs.DeleteOnSubmit(_so);
            try
            {
                db.SubmitChanges();
                message.message = "deleted succesfully";
                message.success = true;
            }
            catch(Exception ex)
            {
                message.success = false;
                message.message = "delete failed " + ex.Message;
            }
            var data = JsonConvert.SerializeObject(message);

            return data;
        }

        public string GetDataSalesOrder(string all)
        {
            var so_list = (from c in db.SALES_SOs 
                           join a in db.COM_CUSTOMERs on c.COM_CUSTOMER_ID equals a.COM_CUSTOMER_ID 
                             select new
                             {
                                 so_id = c.SALES_SO_ID,
                                 so_no = c.SO_NO,
                                 order_date = String.Format("{0:yyyy-MM-dd}", c.ORDER_DATE),
                                 address = c.ADDRESS,
                                 customer = a.CUSTOMER_NAME
                             });
            var data = JsonConvert.SerializeObject(so_list);
            
            return data;
        }

        public string GetDataCustomer(string customername)
        {
            var cust_list = (from c in db.COM_CUSTOMERs
                             select new
                             {
                                 id = c.COM_CUSTOMER_ID,
                                 name = c.CUSTOMER_NAME
                             });
            var data = JsonConvert.SerializeObject(cust_list);            
            return data;
        }


        public string GetSalesOrder(string values)
        {
            int ID = Convert.ToInt32(values);
            db = new SO_ClassesDataContext();
            var sales_order = (from a in db.SALES_SOs
                              where a.SALES_SO_ID == ID
                              select new SalesOrderListContract
                              {
                                  so_id = a.SALES_SO_ID.ToString(),
                                  customer = a.COM_CUSTOMER_ID.ToString(),
                                  sales_address = a.ADDRESS,
                                  sales_order_date = String.Format("{0:yyyy-MM-dd}",a.ORDER_DATE),
                                  sales_order_no = a.SO_NO
                              });
            var data = JsonConvert.SerializeObject(sales_order);

            return data;
        }

        public string GetSalesOrderDetail(string values)
        {
            int ID = Convert.ToInt32(values);
            db = new SO_ClassesDataContext();
            var so_detail = (from b in db.SALES_SO_LITEMs
                             where b.SALES_SO_ID == ID
                             select new SalesOrderDetailContract
                             {
                                 sales_order_id = b.SALES_SO_LITEM_ID.ToString(),
                                 item_name = b.ITEM_NAME,
                                 price = Convert.ToInt32(b.PRICE),
                                 quantity = b.QUANTITY,
                                 total = Convert.ToInt32(b.PRICE) * b.QUANTITY,
                                 isDelete = false
                             });
            var data = JsonConvert.SerializeObject(so_detail);

            return data;
        }
    }

    public class MessageNotif
    {
        public bool success { get; set;  }
        public string message { get; set; }
    }

}
